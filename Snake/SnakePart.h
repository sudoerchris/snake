#pragma once
#include "Point.h"
class SnakePart
{
private:
	Point point = Point(0,0);

public:
	SnakePart* next = nullptr;

	SnakePart(int x, int y);
	SnakePart(Point point);

	Point getPoint();
};