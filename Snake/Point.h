#pragma once
class Point
{
public:
	int x, y;

	Point(int x, int y);

	bool operator==(Point const& point);
};

