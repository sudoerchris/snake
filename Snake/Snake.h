#pragma once
#include "SnakePart.h"
#include "Point.h"
#include "cstdlib"
/// <summary>
/// 
/// </summary>
class Snake
{
public:
	/// <summary>
	/// 
	/// </summary>
	enum class Direction {
		Up,Down,Left,Right
	};

	/// <summary>
	/// 
	/// </summary>
	/// <param name="length"></param>
	/// <returns></returns>
	Snake(int startX, int startY, Direction direction, int length);

	/// <summary>
	/// 
	/// </summary>
	/// <returns></returns>
	int count();

	/// <summary>
	/// move the snake forward 1 step
	/// </summary>
	void forward();

	/// <summary>
	/// 
	/// </summary>
	void grow();

	/// <summary>
	/// 
	/// </summary>
	/// <param name="d"></param>
	void setDirection(Direction d);

	/// <summary>
	/// 
	/// </summary>
	/// <returns></returns>
	Direction* getDirection();

	/// <summary>
	/// 
	/// </summary>
	/// <returns></returns>
	Point nextStep();
	
	/// <summary>
	/// 
	/// </summary>
	/// <returns></returns>
	SnakePart* getHead();

	SnakePart* getTail();

	bool hitSelf();
private:
	SnakePart* head;
	Direction direction;
};

