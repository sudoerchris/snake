#include "Snake.h"

Snake::Snake(int startX, int startY, Direction direction, int length)
{
    this->head = new SnakePart(startX, startY);
    this->direction = direction;
    SnakePart* pt = this->head;
    for (int i = 1; i < length; i++) {
        switch (direction)
        {
        case Snake::Direction::Left:
            pt->next = new SnakePart(startX + i, startY);
            break;
        case Snake::Direction::Right:
            pt->next = new SnakePart(startX - i, startY);
            break;
        case Snake::Direction::Up:
            pt->next = new SnakePart(startX , startY+i);
            break;
        case Snake::Direction::Down:
            pt->next = new SnakePart(startX, startY-i);
            break;
        }
        pt = pt->next;
    }
}

int Snake::count()
{
    SnakePart* cPart = this->head;
    int count = 0;
    while (cPart != nullptr) {
        count++;
        cPart = cPart->next;
    }
    return count;
}

void Snake::forward()
{
    grow();
    SnakePart* newTail = this->head;
    while (newTail->next->next != nullptr) {
        newTail = newTail->next;
    }
    delete(newTail->next);
    newTail->next = nullptr;
}

void Snake::grow()
{
    SnakePart* newHead = new SnakePart(nextStep());
    newHead->next = this->head;
    this->head = newHead;
}

void Snake::setDirection(Direction d)
{
    direction = d;
}

Snake::Direction* Snake::getDirection()
{
    return &(this->direction);
}

Point Snake::nextStep()
{
    Point nextPoint = this->head->getPoint();
    nextPoint.x += (direction == Direction::Left ? -1 : direction == Direction::Right ? 1 : 0);
    nextPoint.y += (direction == Direction::Up ? -1 : direction == Direction::Down ? 1 : 0);
    return nextPoint;
}

SnakePart* Snake::getHead()
{
    return head;
}

SnakePart* Snake::getTail()
{
    SnakePart* tail = getHead();
    while (tail->next != nullptr) {
        tail = tail->next;
    }
    return tail;
}

bool Snake::hitSelf()
{
    Point headPoint = this->head->getPoint();
    SnakePart* nextPart = this->head->next;
    while (nextPart != nullptr) {
        if (headPoint == nextPart->getPoint())
            return true;
        nextPart = nextPart->next;
    }
    return false;
}
