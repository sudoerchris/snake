#include "SnakePart.h"
SnakePart::SnakePart(int x, int y) {
	this->point = Point(x, y);
}

SnakePart::SnakePart(Point point)
{
	this->point = point;
}

Point SnakePart::getPoint()
{
	return Point(point);
}
