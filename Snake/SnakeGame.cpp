﻿// Snake.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <Windows.h>
#include <iomanip>
#include "Snake.h"
#include <thread>
#include <conio.h>
#include <atomic>
#include <random>
#include <ctime>

#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_LEFT 75
#define KEY_RIGHT 77



void moveCursor(int x, int y) {
	printf("\033[%d;%dH", y, x);
}

inline int playarea2ConsoleX(int x) {
	return x + 1;
}

inline int playarea2ConsoleY(int y) {
	return y + 2;
}

void keyInputThread(std::atomic_int* key) {
	while (1)
	{
		*key = _getch();
		if (*key == 'q') {
			return;
		}
	}
}

bool insideSnake(Point p, SnakePart* head) {
	SnakePart* pt = head;
	while (pt!=nullptr) {
		if (pt->getPoint() == p) {
			return true;
		}
		pt = pt->next;
	}
	return false;
}

Point genFood(int width, int height, SnakePart* head) {
	Point food = Point(0, 0);
	while (food.x == 0 || insideSnake(food, head)) {
		std::default_random_engine generator(time(NULL));
		std::uniform_real_distribution<float> genX(1, width+1);
		food.x = (int)genX(generator);
		std::uniform_real_distribution<float> genY(1, height+1);
		food.y = (int)genY(generator);
	}
	return food;
}

int main()
{
	printf("========Snake Game========   Score:0\n");
	int playAreaX = 30;
	int playAreaY = 10;
	int speed = 300;//delay of each step (ms)
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r);
	MoveWindow(console, r.left, r.top, 700,600, TRUE);

	GetWindowRect(console, &r);
	std::cout <<  std::setw((long)playAreaX + 2) << std::setfill('-') <<""<<std::endl;
	for (int i = 1; i <= playAreaY; i++) {
		std::cout<<"|" << std::setw((long)playAreaX + 1) << std::setfill(' ') <<"|" << std::endl;
	}
	std::cout << std::setw((long)playAreaX + 2) << std::setfill('-') << "" << std::endl;
	std::cout << "press q to exit";
	Snake snake = Snake(10, 5, Snake::Direction::Right, 8);
	SnakePart* snakePart = snake.getHead();
	
	while (snakePart != nullptr) {
		Point p = snakePart->getPoint();
		moveCursor(playarea2ConsoleX(p.x), playarea2ConsoleY(p.y));
		if (snakePart == snake.getHead()) {
			std::cout << "@";
		}
		else {
			std::cout << "#";
		}
		snakePart = snakePart->next;
	}
	std::cout<<std::flush;
	std::atomic_int key = 0;
	std::thread keyThread = std::thread(keyInputThread,&key);
	keyThread.detach();
	Point food = genFood(playAreaX,playAreaY,snake.getHead());
	moveCursor(playarea2ConsoleX(food.x), playarea2ConsoleY(food.y));
	std::cout << '*';
	int score = 0;
	while (key!='q') {
		Sleep(speed);

		switch (key) {
		case KEY_UP:
			if (*snake.getDirection() != Snake::Direction::Down) {
				snake.setDirection(Snake::Direction::Up);
			}
			break;
		case KEY_DOWN:
			if (*snake.getDirection() != Snake::Direction::Up) {
				snake.setDirection(Snake::Direction::Down);
			}
			break;
		case KEY_LEFT:
			if (*snake.getDirection() != Snake::Direction::Right) {
				snake.setDirection(Snake::Direction::Left);
			}
			break;
		case KEY_RIGHT:
			if (*snake.getDirection() != Snake::Direction::Left) {
				snake.setDirection(Snake::Direction::Right);
			}
			break;
		default:
			break;
		}


		Point p = snake.getHead()->getPoint();
		moveCursor(playarea2ConsoleX(p.x), playarea2ConsoleY(p.y));
		std::cout << '#';
		if (snake.getHead()->getPoint() == food) {
			snake.grow();
			score++;
			moveCursor(36,1);
			std::cout << score;
			food = genFood(playAreaX, playAreaY, snake.getHead());
			moveCursor(playarea2ConsoleX(food.x), playarea2ConsoleY(food.y));
			std::cout << '*';
		}
		else {
			p = snake.getTail()->getPoint();
			moveCursor(playarea2ConsoleX(p.x), playarea2ConsoleY(p.y));
			std::cout << ' ';
			snake.forward();
		}
		p = snake.getHead()->getPoint();
		moveCursor(playarea2ConsoleX(p.x), playarea2ConsoleY(p.y));
		std::cout << '@';
		moveCursor(1, playAreaY + 5);
		std::cout << std::flush;

		if (snake.hitSelf()) {
			moveCursor(1, playAreaY + 5);
			std::cout << "Snake runs into itself!" << std::endl;
			return EXIT_SUCCESS;
		}
		Point headLoc = snake.getHead()->getPoint();
		if (headLoc.x <= 0 || headLoc.x > playAreaX || headLoc.y <= 0 || headLoc.y > playAreaY) {
			moveCursor(1, playAreaY + 5);
			std::cout << "Snake hits the wall!" << std::endl;
			return EXIT_SUCCESS;
		}
	}
	
	moveCursor(1, playAreaY + 6);
	std::cout << "game ended" << std::endl;
	return EXIT_SUCCESS;
}
