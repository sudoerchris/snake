#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_SnakeQt.h"
#include "Snake.h"

class SnakeQt : public QMainWindow
{
    Q_OBJECT

public:
    SnakeQt(QWidget *parent = Q_NULLPTR);

private:
    Ui::SnakeMainWindow ui;
    Snake snake;
    QTimer* timer = nullptr;
    QCell* foodQCell;
    Point* food;
    QGraphicsScene* scene;
    int score = 0;
};

class arrowKeyReceiver : public QObject {
    Q_OBJECT
public:
	arrowKeyReceiver(Snake* snake) :QObject(), snake(snake) {};
protected:
    bool eventFilter(QObject* obj, QEvent* event);
private:
    Snake* snake;
};