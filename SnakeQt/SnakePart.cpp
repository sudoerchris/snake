#include "SnakePart.h"
SnakePart::SnakePart(int x, int y, bool isHead):
	point(x, y),
	qCell(x,y,isHead?QCell::Type::SnakeHead:QCell::Type::SnakeBody)
{}

SnakePart::SnakePart(Point p, bool isHead):
	SnakePart(p.x,p.y,isHead)
{}

Point SnakePart::getPoint()
{
	return Point(point);
}
QCell* SnakePart::getQCell()
{
	return &qCell;
}
void SnakePart::setPoint(int x, int y)
{
	point.x = x;
	point.y = y;
	qCell.setGamePosToScene(x,y);
}

void SnakePart::setPoint(Point p) {
	setPoint(p.x, p.y);
}