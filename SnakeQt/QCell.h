#pragma once

#include <QtWidgets/qgraphicswidget.h>
#include <QtGui/qpainter.h>
#include "Point.h"
class QCell : public QGraphicsWidget
{
public:
	enum class Type {
		SnakeHead, SnakeBody, Food
	};
	Type type;
	void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*) override;

	void setGamePosToScene(int x, int y);
	QCell(int x, int y, Type t);
	QVariant itemChange(GraphicsItemChange change, const QVariant& value) override;
private:
	int x, y;
};

