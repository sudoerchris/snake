#include "Snake.h"

Snake::Snake(int startX, int startY, Direction direction, int length) :
	direction(direction)
{
	this->head = new SnakePart(startX, startY, true);
	SnakePart* pt = this->head;
	SnakePart* prev = nullptr;
	for (int i = 1; i < length; i++) {
		switch (direction)
		{
		case Snake::Direction::Left:
			pt->next = new SnakePart(startX + i, startY, false);
			break;
		case Snake::Direction::Right:
			pt->next = new SnakePart(startX - i, startY, false);
			break;
		case Snake::Direction::Up:
			pt->next = new SnakePart(startX, startY + i, false);
			break;
		case Snake::Direction::Down:
			pt->next = new SnakePart(startX, startY - i, false);
			break;
		}
		pt->prev = prev;
		prev = pt;
		pt = pt->next;
	}
	pt->prev = prev;
	this->tail = pt;
}

int Snake::count()
{
	SnakePart* cPart = this->head;
	int count = 0;
	while (cPart != nullptr) {
		count++;
		cPart = cPart->next;
	}
	return count;
}

void Snake::forward()
{
	SnakePart* part = this->tail;
	Point nextPoint = nextStep();
	while (part->prev != nullptr) {
		part->setPoint(part->prev->getPoint());
		part = part->prev;
	}
	part->setPoint(nextStep());
}

SnakePart* Snake::grow()
{
	Point tailPoint = tail->getPoint();
	forward();
	SnakePart* newPart = new SnakePart(tailPoint, false);
	tail->next = newPart;
	newPart->prev = tail;
	tail = newPart;
	return newPart;
}

void Snake::setDirection(Direction d)
{
	direction = d;
}

Snake::Direction Snake::getDirection()
{
	return this->direction;
}

Point Snake::nextStep()
{
	Point nextPoint = this->head->getPoint();
	nextPoint.x += (direction == Direction::Left ? -1 : direction == Direction::Right ? 1 : 0);
	nextPoint.y += (direction == Direction::Up ? -1 : direction == Direction::Down ? 1 : 0);
	return nextPoint;
}


bool Snake::isHitSelf()
{
	Point headPoint = this->head->getPoint();
	for (SnakePart* nextPart = this->head->next; nextPart != nullptr; nextPart = nextPart->next) {
		if (headPoint == nextPart->getPoint())
			return true;
	}
	return false;
}

bool Snake::isHitWall(int w, int h) {
	Point p = head->getPoint();
	return p.x < 0 || p.y < 0 || p.x >= w || p.y >= h;
}

bool Snake::isHeadHit(Point p) {
	return head->getPoint() == p;
}
bool Snake::isInside(Point p) {
	for (SnakePart* part = head; part != nullptr; part = part->next) {
		if (part->getPoint() == p) {
			return true;
		}
	}
	return false;
}
SnakePart* Snake::getHead() {
	return head;
}

SnakePart* Snake::getTail() {
	return tail;
}
