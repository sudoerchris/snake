#include "SnakeQt.h"
#include <QtWidgets/qgraphicsscene.h>
#include "QCell.h"
#include <qtimer.h>
#include <qdebug.h>
#include <iostream>
#include <ctime>
#include <conio.h>
#include <random>

bool arrowKeyReceiver::eventFilter(QObject* obj, QEvent* event)
{
	if (event->type() == QEvent::KeyPress) {
		QKeyEvent* key = static_cast<QKeyEvent*>(event);
		switch (key->key()) {
		case Qt::Key_Up:
			qDebug() << "up";
			if (snake->getDirection() != Snake::Direction::Down) {
				snake->setDirection(Snake::Direction::Up);
			}
			return true;
		case Qt::Key_Down:
			qDebug() << "down";
			if (snake->getDirection() != Snake::Direction::Up) {
				snake->setDirection(Snake::Direction::Down);
			}
			return true;
		case Qt::Key_Left:
			qDebug() << "left";
			if (snake->getDirection() != Snake::Direction::Right) {
				snake->setDirection(Snake::Direction::Left);
			}
			return true;
		case Qt::Key_Right:
			qDebug() << "right";
			if (snake->getDirection() != Snake::Direction::Left) {
				snake->setDirection(Snake::Direction::Right);
			}
			return true;
		default:
			return QObject::eventFilter(obj, event);
		};

	}
	else {
		return QObject::eventFilter(obj, event);
	}
	return false;
}

Point genFood(int width, int height, Snake* snake) {
	Point food = Point(-1, -1);
	while (food.x == -1 || snake->isInside(food)) {
		std::default_random_engine generator(time(NULL));
		std::uniform_real_distribution<float> genX(0, width);
		food.x = (int)genX(generator);
		std::uniform_real_distribution<float> genY(0, height);
		food.y = (int)genY(generator);
	}
	return food;
}

SnakeQt::SnakeQt(QWidget* parent) : QMainWindow(parent),
snake(20, 19, Snake::Direction::Right, 8)
{

	//init gui
	setWindowFlags(Qt::Widget | Qt::MSWindowsFixedSizeDialogHint);
	ui.setupUi(this);
	scene = new QGraphicsScene(ui.Playarea->geometry());
	ui.Playarea->setScene(scene);
	scene->setBackgroundBrush(QBrush(Qt::black, Qt::SolidPattern));
	ui.GameOver_Label->setVisible(false);

	//install key event handler
	arrowKeyReceiver* receiver = new arrowKeyReceiver(&snake);
	scene->installEventFilter(receiver);
	//draw playarea
	for (SnakePart* part = snake.getHead(); part != nullptr; part = part->next) {
		scene->addItem(part->getQCell());
	}
	food = new Point(30, 19);
	foodQCell = new QCell(food->x, food->y, QCell::Type::Food);
	scene->addItem(foodQCell);
	foodQCell->setGamePosToScene(food->x, food->y);
	//timer for periodic snake movement
	timer = new QTimer(this);
	//move snake evert tick
	connect(timer, &QTimer::timeout, this, [&]() {
		Point nextStep = snake.nextStep();
		if (nextStep == *food) { //hit food
			//delete(food);
			food = new Point(genFood(60, 40, &snake));
			SnakePart* newpart = snake.grow();
			scene->addItem(newpart->getQCell());
			score++;
			ui.Score_Label->setText(QString::number(score));
			foodQCell->setGamePosToScene(food->x, food->y);
		}
		else if (snake.isInside(nextStep) || nextStep.x < 0 || nextStep.x >= 60 || nextStep.y < 0 || nextStep.y >= 40) { //hit game over condition
			snake.forward();
			timer->stop();
			ui.GameOver_Label->setVisible(true);
		}
		else { //normal movement
			snake.forward();
		}

		});

	setEnabled(true);
	timer->start(200);
}
