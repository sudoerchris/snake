#include "Point.h"

Point::Point(int x, int y)
{
	this->x = x;
	this->y = y;
}

bool Point::operator==(Point const& point)
{
		return x == point.x && y == point.y;
}

