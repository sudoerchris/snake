#pragma once
#include "SnakePart.h"
#include "Point.h"
#include "cstdlib"
#include <atomic>
/// <summary>
/// class for snake operation
/// </summary>
class Snake
{
public:
	/// <summary>
	/// Direction of the snake
	/// </summary>
	enum class Direction {
		Up,Down,Left,Right
	};

	/// <summary>
	/// constructor
	/// </summary>
	/// <param name="startX">head X</param>
	/// <param name="startY">head Y</param>
	/// <param name="direction">snake heading</param>
	/// <param name="length">snake length</param>
	/// <returns></returns>
	Snake(int startX, int startY, Direction direction, int length);

	/// <summary>
	/// count the length
	/// </summary>
	/// <returns>length</returns>
	int count();

	/// <summary>
	/// move the snake forward 1 step
	/// </summary>
	void forward();

	/// <summary>
	/// move forward and increase length
	/// </summary>
	/// <returns>new part</returns>
	SnakePart* grow();

	/// <summary>
	/// 
	/// </summary>
	/// <param name="d"></param>
	void setDirection(Direction d);

	/// <summary>
	/// 
	/// </summary>
	/// <returns></returns>
	Direction getDirection();

	/// <summary>
	/// next position of head
	/// </summary>
	/// <returns>point of next step</returns>
	Point nextStep();
	
	/// <summary>
	/// if the head hit the body
	/// </summary>
	/// <returns>true if hit</returns>
	bool isHitSelf();

	/// <summary>
	/// if the head hit the wall
	/// </summary>
	/// <returns>true if hit</returns>
	bool isHitWall(int w, int h);

	/// <summary>
	/// if the head hit the point
	/// </summary>
	/// <param name="p">point for check</param>
	/// <returns>true if hit</returns>
	bool isHeadHit(Point p);

	/// <summary>
	/// if the point overlap with the snake
	/// </summary>
	/// <param name="p">point for check</param>
	/// <returns>true if overlap</returns>
	bool isInside(Point p);

	SnakePart* getHead();

	SnakePart* getTail();
private:
	SnakePart *head = nullptr,*tail = nullptr;
	std::atomic<Direction> direction;
};

