#pragma once
#include "Point.h"
#include <QtWidgets/qgraphicswidget.h>
#include "QCell.h"

class SnakePart
{
private:
	QCell qCell;
	Point point;

public:
	SnakePart* next = nullptr;
	SnakePart* prev = nullptr;
	SnakePart(int x, int y, bool isHead);
	SnakePart(Point point, bool isHead);

	Point getPoint();
	QCell* getQCell();
	void setPoint(int x, int y);
	void setPoint(Point p);

};