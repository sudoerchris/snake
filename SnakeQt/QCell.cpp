#include "QCell.h"
#include <QtWidgets/qgraphicsscene.h>
void QCell::paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*)
{
    switch (type)
    {
    case Type::SnakeHead:
        painter->fillRect(rect(), Qt::lightGray);
        painter->setPen(Qt::NoPen);
        painter->setBrush(QColor(Qt::red));
        painter->drawEllipse(QPointF(5, 5), 2, 2);
        break;
    case Type::SnakeBody:
        painter->fillRect(rect(), Qt::white);
        break;
    case Type::Food:
        painter->fillRect(rect(), Qt::yellow);
        break;
    default:
        break;
    }
}

void QCell::setGamePosToScene(int x, int y)
{
    this->x = x;
    this->y = y;
    if (scene() != nullptr) {
        QRectF sceneRect = scene()->sceneRect();
        setGeometry(sceneRect.x() + (long)x * 10, sceneRect.y() + (long)y * 10, 10, 10);
    }
 
}

QCell::QCell(int x, int y, Type t):
    QGraphicsWidget(),
    type(t),
    x(x),
    y(y)
{
    setGamePosToScene(x, y);
    if (t == Type::Food) {
        setZValue(-1);
    }
    else if (t == Type::SnakeHead) {
        setZValue(1);
    }
}

QVariant QCell::itemChange(GraphicsItemChange change, const QVariant& value)
{
    if (change == GraphicsItemChange::ItemSceneHasChanged && scene() != nullptr) {
        setGamePosToScene(x, y);
    }
    return QGraphicsWidget::itemChange(change,value);
}
